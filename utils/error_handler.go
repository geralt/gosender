package utils

import "fmt"

func ProcessIfError(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}
