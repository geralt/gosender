package config

import (
	"go.sender/utils"
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Mysql       MysqlConfig `yaml:"mysql"`
	HTTP        HTTPConfig  `yaml:"http"`
	APIToken    string      `yaml:"api_token"`
	Workers     int         `yaml:"workers"`
	Environment string      `yaml:"environment"`
}

// MysqlConfig is a config for mysql
type MysqlConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

// HTTPConfig http binding config
type HTTPConfig struct {
	IP   string `yaml:"ip"`
	Port int    `yaml:"port"`
}

func Read() *Config {
	file, err := os.Open("config.yml")
	utils.ProcessIfError(err)

	var config *Config

	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(&config)
	utils.ProcessIfError(err)

	return config
}
