module go.sender

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	gopkg.in/yaml.v2 v2.3.0
)
