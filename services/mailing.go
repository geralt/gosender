package services

import (
	"fmt"
	"go.sender/config"
	"go.sender/repositories"
	"sync"
	"sync/atomic"
)

type Stats struct {
	Success int32
	Error   int32
	Timeout int32
}

func Mailing(config *config.Config, records []repositories.Email) Stats {
	var statsWG sync.WaitGroup
	statsChannel := make(chan string)

	chunkSize := 1000
	for j := 0; j < len(records); j += chunkSize {
		end := j + chunkSize

		if end > len(records) {
			end = len(records)
		}

		part := records[j:end]
		go func(emails []repositories.Email) {
			statsWG.Add(len(emails))
			for _, item := range emails {
				statsChannel <- Send(item.Email)
			}
		}(part)
	}

	var stats Stats
	for i := 1; i <= config.Workers; i++ {
		go CountStats(i, &stats, statsChannel, &statsWG)
	}

	statsWG.Wait()

	return stats
}

func CountStats(id int, stats *Stats, statsChannel chan string, wg *sync.WaitGroup) {
	for status := range statsChannel {
		if status == "sent" {
			fmt.Printf("Worker %d: success +1\n", id)
			atomic.AddInt32(&stats.Success, 1)
		}
		if status == "error" {
			fmt.Printf("Worker %d: error +1\n", id)
			atomic.AddInt32(&stats.Error, 1)
		}
		if status == "timeout" {
			fmt.Printf("Worker %d: timeout +1\n", id)
			atomic.AddInt32(&stats.Timeout, 1)
		}
		wg.Done()
	}
	return
}
