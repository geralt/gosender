package services

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"go.sender/config"
	"go.sender/utils"
)

func EstablishDatabaseConnection(config config.MysqlConfig) *sql.DB {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true", config.User, config.Password, config.Host, config.Database)
	db, err := sql.Open("mysql", connectionString)
	utils.ProcessIfError(err)

	return db
}
