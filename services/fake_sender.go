package services

import (
	"math/rand"
	"time"
)

func Send(emails string) string {
	timeout := rand.Intn(15)
	time.Sleep(time.Duration(timeout) * time.Millisecond)

	variants := []string{
		"sent",
		"error",
		"timeout",
	}

	index := rand.Intn(len(variants))
	rndString := variants[index]

	return rndString
}
