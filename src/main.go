package main

import (
	"fmt"
	"go.sender/config"
	"go.sender/repositories"
	"go.sender/services"
	"time"
)

func main() {
	timeStart := time.Now()
	appConfig := config.Read()
	db := services.EstablishDatabaseConnection(appConfig.Mysql)

	defer db.Close()

	records := repositories.GetEmailRecors(db)

	stats := services.Mailing(appConfig, records)

	fmt.Println(stats)
	fmt.Printf("Elapsed time: %v", time.Since(timeStart))
}
