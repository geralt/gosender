package repositories

import (
	"database/sql"
	"go.sender/utils"
)

type Email struct {
	Id    int
	Email string
}

func GetEmailRecors(db *sql.DB) []Email {
	records := make([]Email, 0)

	maxId := GetMaxID(db)
	bucketSize := 100000

	resultChannel := make(chan []Email, 0)
	resultCount := 0

	for beginID := 1; beginID <= maxId; beginID += bucketSize {
		endId := beginID + bucketSize

		if endId > maxId {
			endId = maxId
		}

		go func(beginId int, endId int) {
			currentRecords := make([]Email, 0)
			stmt, err := db.Prepare("SELECT id, email FROM emails WHERE id >= ? AND id < ?")
			utils.ProcessIfError(err)

			defer stmt.Close()

			results, err := stmt.Query(beginId, endId)
			utils.ProcessIfError(err)

			if results != nil {
				for results.Next() {
					var record Email

					err = results.Scan(&record.Id, &record.Email)
					utils.ProcessIfError(err)

					currentRecords = append(currentRecords, record)
				}

				resultChannel <- currentRecords
			}
		}(beginID, endId)
		resultCount += 1
	}

	for i := 0; i < resultCount; i++ {
		currentRecords := <-resultChannel
		records = append(records, currentRecords...)
	}

	return records
}

func GetMaxID(db *sql.DB) int {
	row := db.QueryRow("SELECT MAX(`id`) FROM emails")

	var maxID int
	err := row.Scan(&maxID)
	utils.ProcessIfError(err)

	return maxID
}
